﻿using System;
using System.Collections.Generic;
using UnityEngine;
using KartGame.KartSystems;

public class ObjectiveManager : MonoBehaviour
{
    List<Objective> m_Objectives = new List<Objective>();

    public List<Objective> Objectives => m_Objectives;

    public static Action<Objective> RegisterObjective;

    public void OnEnable()
    {
        RegisterObjective += OnRegisterObjective;
    }
    
    public bool AreAllObjectivesCompleted()
    {
        ArcadeKart player = GameObject.Find("KartClassic_Player").gameObject.GetComponent(typeof(ArcadeKart)) as ArcadeKart;
        return player.lapCount == 3;
    }

    public void OnRegisterObjective(Objective objective)
    {
        m_Objectives.Add(objective);
    }
}
