using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using KartGame.KartSystems;

public class LapDisplay : MonoBehaviour
{
    private int laps = 0;
    public Text lapText;
    public GameObject kart;
    private ArcadeKart ks;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        kart = GameObject.Find("KartClassic_Player");
        if (kart != null)
        {
            Debug.Log("kart is not null");
            ks = kart.gameObject.GetComponent(typeof(ArcadeKart)) as ArcadeKart;
        }
        if (ks != null)
        {
            Debug.Log("ks is not null");
            laps = ks.get_laps();
        }
        lapText.text = "Laps: " + laps + "/3";
    }
}