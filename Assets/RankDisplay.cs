﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using KartGame.KartSystems;

public class RankDisplay : MonoBehaviour
{
    private int rank = 0;
    private int totalPlayers = 0;
    public Text rankText;
    public GameObject kart;
    private ArcadeKart ks;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        kart = GameObject.Find("KartClassic_Player");
        if (kart != null)
        {
            Debug.Log("kart is not null");
            ks = kart.gameObject.GetComponent(typeof(ArcadeKart)) as ArcadeKart;
        }
        if (ks != null)
        {
            Debug.Log("ks is not null");
            rank = ks.get_rank();
            totalPlayers = ks.get_totalPlayers();
        }
        rankText.text = "Position: " + rank + "/" + totalPlayers;
    }
}