using UnityEngine;
using System.Collections;
using System.Collections.Generic;


//Interpolation between points with a Catmull-Rom spline
public class CatmullRomSpline : MonoBehaviour
{
	//Has to be at least 4 points
	public Transform[] controlPointsList;
	//Are we making a line or a loop?
	public bool isLooping = true;
    public float road_width = 10f;

	public List<Vector3> road_vertices;
	public List<Vector3> left_wall_vertices;
	public List<Vector3> right_wall_vertices;

	Transform jarvis;

	public CatmullRomSpline(Transform[] controlPointsList, float road_width, Transform jarvis){
		this.jarvis = jarvis;
		this.controlPointsList = controlPointsList;
		this.road_width = road_width;
	}

	//Display without having to press play
	public void create_spline_and_mesh()
	{
		road_vertices = new List<Vector3>();
		left_wall_vertices = new List<Vector3>();
		right_wall_vertices = new List<Vector3>();


		//Draw the Catmull-Rom spline between the points
		for (int i = 0; i < controlPointsList.Length; i++)
		{
			//Cant draw between the endpoints
			//Neither do we need to draw from the second to the last endpoint
			//...if we are not making a looping line
			if ((i == 0 || i == controlPointsList.Length - 2 || i == controlPointsList.Length - 1) && !isLooping)
			{
				continue;
			}

			build_segment(i);
		}

		TrackMeshGen trackMeshGen = new TrackMeshGen(jarvis);
		trackMeshGen.Start();
		trackMeshGen.CreateRoad(road_vertices);
		trackMeshGen.CreateWalls(left_wall_vertices, right_wall_vertices);
	}

	//Display a spline between 2 points derived with the Catmull-Rom spline algorithm
	void build_segment(int pos)
	{
		//The 4 points we need to form a spline between p1 and p2
		Vector3 p0 = controlPointsList[ClampListPos(pos - 1)].position;
		Vector3 p1 = controlPointsList[pos].position;
		Vector3 p2 = controlPointsList[ClampListPos(pos + 1)].position;
		Vector3 p3 = controlPointsList[ClampListPos(pos + 2)].position;

		//The start position of the line
		Vector3 lastPos = p1;

		//The spline's resolution
		//Make sure it's is adding up to 1, so 0.3 will give a gap, but 0.2 will work
		float resolution = 0.05f;

		//How many times should we loop?
		int loops = Mathf.FloorToInt(1f / resolution);


		for (int i = 1; i <= loops; i++)
		{
			//Which t position are we at?
			float t = i * resolution;

			//Find the coordinate between the end points with a Catmull-Rom spline
			Vector3 newPos = GetCatmullRomPosition(t, p0, p1, p2, p3);

			//Draw this line segment
            //Debug.Log("Last Pos: " + lastPos + ", New Pos: " + newPos);
			// Gizmos.DrawLine(lastPos, newPos);
			// Gizmos.color = Color.red;
			// Gizmos.DrawSphere(newPos, .075f);


			Vector3 derivative = Quaternion.AngleAxis(90, Vector3.up) * GetDerivative(t, p0, p1, p2, p3);
			derivative = derivative.normalized;
			Vector3 left_road_vertex = newPos + derivative * road_width;
			Vector3 right_road_vertex = newPos - derivative * road_width;
			road_vertices.Add(left_road_vertex);
			road_vertices.Add(right_road_vertex);

			Vector3 left_wall_vertex = new Vector3(0,road_width/4f,0) + newPos + derivative * road_width;
			Vector3 right_wall_vertex = new Vector3(0,road_width/4f,0) + newPos - derivative * road_width;
			left_wall_vertices.Add(left_road_vertex);
			left_wall_vertices.Add(left_wall_vertex);
			right_wall_vertices.Add(right_road_vertex);
			right_wall_vertices.Add(right_wall_vertex);

			//DEBUG DRAW ROAD
			// Gizmos.color = Color.yellow;
			// Gizmos.DrawSphere(left_road_vertex, .1f);
			// Gizmos.DrawSphere(left_wall_vertex, .05f);
			// Gizmos.color = Color.green;
			// Gizmos.DrawSphere(right_road_vertex, .1f);
			// Gizmos.DrawSphere(right_wall_vertex, .05f);
			// Gizmos.color = Color.red;
			//Save this pos so we can draw the next line segment
			lastPos = newPos;
		}
	}

	//Clamp the list positions to allow looping
	int ClampListPos(int pos)
	{
		if (pos < 0)
		{
			pos = controlPointsList.Length - 1;
		}

		if (pos > controlPointsList.Length)
		{
			pos = 1;
		}
		else if (pos > controlPointsList.Length - 1)
		{
			pos = 0;
		}

		return pos;
	}

	//Returns a position between 4 Vector3 with Catmull-Rom spline algorithm
	//http://www.iquilezles.org/www/articles/minispline/minispline.htm
	Vector3 GetCatmullRomPosition(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
	{
		//The coefficients of the cubic polynomial (except the 0.5f * which I added later for performance)
		Vector3 a = 2f * p1;
		Vector3 b = p2 - p0;
		Vector3 c = 2f * p0 - 5f * p1 + 4f * p2 - p3;
		Vector3 d = -p0 + 3f * p1 - 3f * p2 + p3;

		//The cubic polynomial: a + b * t + c * t^2 + d * t^3
		Vector3 pos = 0.5f * (a + (b * t) + (c * t * t) + (d * t * t * t));

		return pos;
	}

	public Vector3 GetDerivative(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3){
		//The coefficients of the cubic polynomial (except the 0.5f * which I added later for performance)
		Vector3 a = 2f * p1;
		Vector3 b = p2 - p0;
		Vector3 c = 2f * p0 - 5f * p1 + 4f * p2 - p3;
		Vector3 d = -p0 + 3f * p1 - 3f * p2 + p3;

		//The derivative: b + 2c*t + 3d*t^2
		Vector3 deriv = 0.5f * (b + (2 * c * t) + (3 * d * t * t));

		return deriv;
	}
}