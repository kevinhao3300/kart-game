/*Please do support www.bitshiftprogrammer.com by joining the facebook page : fb.com/BitshiftProgrammer
Legal Stuff:
This code is free to use no restrictions but attribution would be appreciated.
Any damage caused either partly or completly due to usage of this stuff is not my responsibility*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using KartGame.KartSystems;

public class CustomVectorComparer : IEqualityComparer<Vector3> {
    public bool Equals(Vector3 v1, Vector3 v2) {
        if (v1 == null && v2 == null) { return true;}
        if (v1 == null | v2 == null) { return false;}
        if (v1.Equals(v2)) { return true; }
        return false;
    }
        
    public int GetHashCode(Vector3 v) {
        string code = v.x+","+v.y+","+v.z;
        return code.GetHashCode();
    }
}

public class JarvisMarchConvexHull : MonoBehaviour 
{
    public Transform[] points;

    [Range(0.0f,1000f)]
    public float x_range = 0.0f;

    [Range(0.0f,1000f)]
    public float z_range = 0.0f;
    
    private HashSet<Transform> result;

    private HashSet<Vector3> valid_points;

    public GameObject player;

    public int numBots;

    public GameObject startLine;

    Vector3 first_pos;

    void Start()
    {
        GameObject camera = GameObject.Find("PreviewCamera");
        camera.transform.position = new Vector3(x_range / 2, 400, z_range / 2);
        camera.transform.forward = Vector3.down;
        GameObject canvas = GameObject.Find("Canvas");
        canvas.SetActive(true);
        
        // Randomly assign the points
        result = new HashSet<Transform>();
        valid_points = new HashSet<Vector3>(new CustomVectorComparer());
        float xSum = 0;
        float zSum = 0;
        for (int i = 0; i < points.Length; i++) {
            GameObject emptyGO = new GameObject();
            points[i] = emptyGO.transform;
            float new_x = 0f;
            float new_z = 0f;
    
            while(true) {
                new_x = Random.Range(0f, x_range);
                new_z = Random.Range(0f, z_range);
                Vector3 newPos = new Vector3(Mathf.Round(new_x) - (Mathf.Round(new_x) % 35), 0f, Mathf.Round(new_z) - (Mathf.Round(new_z) % 35));
                if (!valid_points.Contains(newPos)) {
                    valid_points.Add(newPos);
                    points[i].position += newPos;
                    xSum += new_x;
                    zSum += new_z;
                    break;
                }
            }
        }
        
        create_hull();
        //Debug.Log("The size of result at the end of create_hull in Start() is: " + result.Count);
        List<Transform> outer = new List<Transform>();
        foreach (var item in result) {
            Debug.Log(item);
            outer.Add(item);
        }

        first_pos = outer[0].position;
        
        first_pos.y += 1f;


        startLine.transform.position = new Vector3(first_pos.x, 0.0001f, first_pos.z);

        //Debug.Log("outer's size: " + outer.Count);
        CatmullRomSpline spline = new CatmullRomSpline(outer.ToArray(), 5f, this.transform);
        spline.create_spline_and_mesh();

        // drawn
        
        Quaternion derivative =  Quaternion.LookRotation(spline.GetDerivative(0f, outer[outer.Count-1].position,first_pos,outer[1].position,outer[2].position), Vector3.up);
        player.transform.rotation = derivative;

        player.transform.position = first_pos - player.transform.forward * 3;


        startLine.transform.rotation = derivative;
        GameObject.Find("Center").transform.position = new Vector3(xSum / points.Length, 0.0f, zSum / points.Length);

        GameObject AI_manager = GameObject.Find("AI_manager");
        for (int i = 0; i < AI_manager.transform.childCount; ++i) {
            GameObject curBot = GameObject.Find("KartAI" + i);

            if (i >= numBots)
                curBot.SetActive(false);
            else {
                curBot.transform.rotation = derivative;
                int noise = i % 2 == 0 ? 2 : -2;
                curBot.transform.position = first_pos - (curBot.transform.forward * ((i + 1))) + (curBot.transform.right * noise);
            }
        }
    }

    public void remakeTrack() {
        for (int i = 0; i < points.Length; ++i) {
            Destroy(points[i].gameObject);
        }
        result.Clear();

        Debug.Log("remake track");
        // Randomly assign the points
        result = new HashSet<Transform>();
        valid_points = new HashSet<Vector3>(new CustomVectorComparer());
        float xSum = 0;
        float zSum = 0;
        for (int i = 0; i < points.Length; i++) {
            GameObject emptyGO = new GameObject();
            points[i] = emptyGO.transform;
            float new_x = 0f;
            float new_z = 0f;
    
            while(true) {
                new_x = Random.Range(0f, x_range);
                new_z = Random.Range(0f, z_range);
                Vector3 newPos = new Vector3(Mathf.Round(new_x) - (Mathf.Round(new_x) % 35), 0f, Mathf.Round(new_z) - (Mathf.Round(new_z) % 35));
                if (!valid_points.Contains(newPos)) {
                    valid_points.Add(newPos);
                    points[i].position += newPos;
                    xSum += new_x;
                    zSum += new_z;
                    break;
                }
            }
        }
        

        create_hull();
        //Debug.Log("The size of result at the end of create_hull in Start() is: " + result.Count);
        List<Transform> outer = new List<Transform>();
        foreach (var item in result) {
            Debug.Log(item);
            outer.Add(item);
        }

        first_pos = outer[0].position;
        
        first_pos.y += 1f;
        player.transform.position = first_pos;

        startLine.transform.position = new Vector3(first_pos.x, 0.0001f, first_pos.z);

        //Debug.Log("outer's size: " + outer.Count);
        CatmullRomSpline spline = new CatmullRomSpline(outer.ToArray(), 5f, this.transform);
        spline.create_spline_and_mesh();

        // drawn
        
        Quaternion derivative =  Quaternion.LookRotation(spline.GetDerivative(0f, outer[outer.Count-1].position,first_pos,outer[1].position,outer[2].position), Vector3.up);
        player.transform.rotation = derivative;

        player.transform.position = first_pos - player.transform.forward * 3;


        startLine.transform.rotation = derivative;
        GameObject.Find("Center").transform.position = new Vector3(xSum / points.Length, 0.0f, zSum / points.Length);

        GameObject AI_manager = GameObject.Find("AI_manager");
        for (int i = 0; i < AI_manager.transform.childCount; ++i) {
            GameObject curBot = GameObject.Find("KartAI" + i);

            if (i >= numBots)
                curBot.SetActive(false);
            else {
                curBot.transform.rotation = derivative;
                int noise = i % 2 == 0 ? 2 : -2;
                curBot.transform.position = first_pos - (curBot.transform.forward * ((i + 1))) + (curBot.transform.right * noise);
            }
        }
    }

    // private void Update() {
    //     if (first_pos == null) {
    //         return;
    //     }

    //     RaycastHit hit;
    //     if (Physics.Raycast(first_pos + Vector3.up, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity))
    //     {
    //         Debug.DrawRay(first_pos + Vector3.up, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
    //         Debug.Log("Did Hit");
    //     }
    //     else
    //     {
    //         Debug.DrawRay(first_pos + Vector3.up, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
    //         Debug.Log("Did not Hit");
    //     }
    // }

    // void OnDrawGizmos(){ //draw range in which random points can be placed
    //     Gizmos.color = Color.magenta;
    //     // Gizmos.DrawCube(this.transform.position, new Vector3(x_range, 0.1f,
    //     // z_range));
    //     for (int i = 0; i < points.Length; i++)
    //         if(points[i] != null)
    //             Gizmos.DrawSphere( points[i].position, .5f);
    // }

    void create_hull()
    {
        int leftMostIndex = 0;
        for (int i = 1; i < points.Length; i++)
        {
            if (points[leftMostIndex].position.x > points[i].position.x)
                leftMostIndex = i;
        }
        result.Add(points[leftMostIndex]);
        List<Transform> collinearPoints = new List<Transform>();
        Transform current = points[leftMostIndex];
        while (true)
        {
            Transform nextTarget = points[0];
            for (int i = 1; i < points.Length; i++)
            {
                if (points[i] == current)
                    continue;
                float x1, x2, y1, y2;
                x1 = current.position.x - nextTarget.position.x;
                x2 = current.position.x - points[i].position.x;

                y1 = current.position.z - nextTarget.position.z;
                y2 = current.position.z - points[i].position.z;

                float val = (y2 * x1) - (y1 * x2);
                if (val > 0)
                {
                    nextTarget = points[i];
                    collinearPoints = new List<Transform>();
                }
                else if (val == 0)
                {
                    if (Vector3.Distance(current.position, nextTarget.position) < Vector3.Distance(current.position, points[i].position))
                    {
                        collinearPoints.Add(nextTarget);
                        nextTarget = points[i];
                    }
                    else
                        collinearPoints.Add(points[i]);                    
                }
            }

            foreach (Transform t in collinearPoints)
                result.Add(t);            
            if (nextTarget == points[leftMostIndex])
                break;
            result.Add(nextTarget);
            current = nextTarget;
        }
        //Debug.Log("The size of result at the end of create_hull is: " + result.Count);
    }

    // void update_hull_and_spline()
    // {
    //     Update();
    //     if (drawIt)
    //     {
    //         if (result != null)
    //         {
    //             Debug.Log("in here");
    //             List<Transform> outer = new List<Transform>();
    //             foreach (var item in result)
    //                 outer.Add(item);
    //             for (int i = 0; i < outer.Count - 1; i++)
    //                 Gizmos.DrawLine(outer[i].position, outer[i + 1].position);            
    //             Gizmos.DrawLine(outer[0].position, outer[outer.Count - 1].position);
    //             CatmullRomSpline spline = new CatmullRomSpline(outer.ToArray(), 0.2f, this.transform);
    //             spline.OnDrawGizmos();
    //         }

    //         for (int i = 0; i < points.Length; i++)
    //         {
    //             Gizmos.color = Color.cyan;
    //             if (result != null)
    //             {
    //                 if (result.Contains(points[i]))
    //                     Gizmos.color = Color.yellow;
    //             }
    //             Gizmos.DrawSphere(points[i].position, size);
    //         }
    //     }
    // }
}