using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// [RequireComponent(typeof(MeshFilter))]
public class TrackMeshGen {
    Mesh road_mesh;
    MeshFilter road_mesh_filter;
    MeshCollider road_mesh_collider;

    Mesh left_wall_mesh;

    MeshFilter left_wall_mesh_filter;

    MeshCollider left_wall_mesh_collider;

    
    Mesh right_wall_mesh;

    MeshFilter right_wall_mesh_filter;

    MeshCollider right_wall_mesh_collider;

    Transform jarvis;

    public TrackMeshGen(Transform jarvis){
        this.jarvis = jarvis;
    }

    public void Start() {
        road_mesh = new Mesh();
        left_wall_mesh = new Mesh();
        right_wall_mesh = new Mesh();

        road_mesh_filter = jarvis.Find("RoadMesh").gameObject.GetComponent(typeof(MeshFilter)) as MeshFilter;
        road_mesh_collider = jarvis.Find("RoadMesh").gameObject.GetComponent(typeof(MeshCollider)) as MeshCollider;
        left_wall_mesh_filter = jarvis.Find("LeftWallMesh").gameObject.GetComponent(typeof(MeshFilter)) as MeshFilter;
        right_wall_mesh_filter = jarvis.Find("RightWallMesh").gameObject.GetComponent(typeof(MeshFilter)) as MeshFilter;
        left_wall_mesh_collider = jarvis.Find("LeftWallMesh").gameObject.GetComponent(typeof(MeshCollider)) as MeshCollider;
        right_wall_mesh_collider = jarvis.Find("RightWallMesh").gameObject.GetComponent(typeof(MeshCollider)) as MeshCollider;

    }

    public void CreateRoad(List<Vector3> road_vertices) {
        //making triangles
        if(road_vertices.Count % 2 != 0)
            Debug.Log("Vertices Array to Create Road NOT Modulo 2");

        road_mesh.vertices = road_vertices.ToArray();
        road_mesh.triangles = trianglesHelper(road_vertices);
        road_mesh_filter.mesh = road_mesh;
        road_mesh_collider.sharedMesh = road_mesh;
    }

    public void CreateWalls(List<Vector3> left_wall_vertices, List<Vector3> right_wall_vertices) {
        int[] left_triangles = trianglesHelper(left_wall_vertices);
        int[] right_triangles = trianglesHelper(right_wall_vertices);
        
        //var wall_triangles = new int[left_triangles.Length + right_triangles.Length];

       //Debug.Log("This is the size of left_triangles: " + left_triangles.Length);
       // Debug.Log("This is the size of right_triangles: " + right_triangles.Length);
        //Debug.Log("This is the size of wall_triangles: " + wall_triangles.Length);



        //left_triangles.CopyTo(wall_triangles, 0);
        //right_triangles.CopyTo(wall_triangles, left_triangles.Length);


        //Debug.Log("This is the size of left_triangles_vertices: " + left_wall_vertices.Count);
        //Debug.Log("This is the size of right_triangles_vertices: " + right_wall_vertices.Count);
        
        //List<Vector3> wall_vertices = new List<Vector3>();
        //wall_vertices.AddRange(left_wall_vertices);
        //wall_vertices.AddRange(right_wall_vertices);
        //Debug.Log("This is the size of wall_triangles_vertices: " + wall_vertices.Count);
        left_wall_mesh.vertices = left_wall_vertices.ToArray();
        left_wall_mesh.triangles = left_triangles;
        left_wall_mesh_filter.mesh = left_wall_mesh;
        left_wall_mesh_collider.sharedMesh = left_wall_mesh;

        right_wall_mesh.vertices = right_wall_vertices.ToArray();
        right_wall_mesh.triangles = right_triangles;
        right_wall_mesh_filter.mesh = right_wall_mesh;
        right_wall_mesh_collider.sharedMesh = right_wall_mesh;
    }

    private int[] trianglesHelper(List<Vector3> vertices) {
        int[] triangles = new int[vertices.Count * 6];
        int i = 0;
        int t_i = 0;
        for (; i + 3 < vertices.Count; i += 2) {
            //left triangle top mesh
            triangles[t_i++] = i+1; //bottom right
            triangles[t_i++] = i+2; //top left
            triangles[t_i++] = i;   //bottom left

            //bottom
            triangles[t_i++] = i;   //bottom left
            triangles[t_i++] = i+2; //top left
            triangles[t_i++] = i+1; //bottom right

            //top
            triangles[t_i++] = i+2; // top left
            triangles[t_i++] = i+1; //bottom right
            triangles[t_i++] = i+3; //top right

            //bottom
            triangles[t_i++] = i+3;   //top right
            triangles[t_i++] = i+1; //top left
            triangles[t_i++] = i+2; // top left
        }
        Debug.Log("This is vertices size: " + vertices.Count);
        Debug.Log("This is t_i: " + t_i);
        //connect the last two vertices to the first two
        triangles[t_i++] = i+1; //bottom right
        triangles[t_i++] = 0; //top left
        triangles[t_i++] = i; //bottom left

        //bottom
        triangles[t_i++] = i;   //bottom left
        triangles[t_i++] = 0; //top left
        triangles[t_i++] = i+1; //bottom right

        triangles[t_i++] = 0; // top left
        triangles[t_i++] = i+1; //bottom right
        triangles[t_i++] = 1; //top right

        triangles[t_i++] = 1; //top right
        triangles[t_i++] = i+1; //bottom right
        triangles[t_i++] = 0; // top left
        return triangles;
    }
    
}